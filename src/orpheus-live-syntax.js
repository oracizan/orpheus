OrpheusLiveSyntax = (function() {
    var syntaxTimeout = null;
    const KIND_GMLSCRIPT = 0;
    const KIND_MULTIFILE = 1;
    const KIND_EVENTS = 2;
    const KIND_ROOMCCS = 3;
    const KIND_GLSL = 4;
    const KIND_KHLSL = 5;

    function doSyntaxUpdate(session) {
        // Get required Haxe classes
        var parsersGmlEvent = $hxClasses["parsers.GmlEvent"];
        var parsersGmlSeeker = $hxClasses["parsers.GmlSeeker"];
        var fileKindYyKYyEvents = $hxClasses["file.kind.yy.KYyEvents"];
        var parsersGmlSeekData = $hxClasses["parsers.GmlSeekData"];
        //var parsersGmlExtArgs = $hxClasses["parsers.GmlExtArgs"];
        var synextGmlExtArgs = $hxClasses["synext.GmlExtArgs"];

        // Get session data
        var gmlFile = session.gmlFile,
            gmlCode = session.getValue();

        // Set file kind
        var kind = null;
        if (gmlFile.kind instanceof $hxClasses["file.kind.gml.KGmlScript"]) kind = KIND_GMLSCRIPT;
        if (gmlFile.kind instanceof $hxClasses["file.kind.gml.KGmlMultifile"]) kind = KIND_MULTIFILE;
        if (gmlFile.kind instanceof $hxClasses["file.kind.yy.KYyEvents"]) kind = KIND_EVENTS;
        if (gmlFile.kind instanceof $hxClasses["file.kind.yy.KYyRoomCCs"]) kind = KIND_ROOMCCS;
        //if (gmlFile.kind instanceof $hxClasses["file.kind.misc.KGLSL"]) kind = KIND_GLSL;
        //if (gmlFile.kind instanceof $hxClasses["file.kind.misc.KHLSL"]) kind = KIND_KHLSL;
        if (kind === null) return;

        // Process Syntax
        switch (kind) {
            case KIND_GMLSCRIPT:
                // Post-process
                gmlCode = $hxClasses["file.kind.KGml"].prototype.postproc.call(gmlFile, gmlFile.editor, gmlCode);
                if (gmlCode === null) return;
                gmlCode = $hxClasses["file.kind.gml.KGmlScript"].prototype.postproc_1(gmlFile.editor, gmlCode);
                if (gmlCode === null) return;

                // Check Syntax
                parsersGmlSeeker.runSync(gmlFile.path, gmlCode, null, gmlFile.kind);
                break;
            case KIND_MULTIFILE:
                // Post-process
                gmlCode = $hxClasses["file.kind.KGml"].prototype.postproc.call(gmlFile, gmlFile.editor, gmlCode);
                if (gmlCode === null) return;
                gmlCode = synextGmlExtArgs.post(gmlCode);
                if (gmlCode === null) return;

                // Check Syntax
                parsersGmlSeeker.runSync(gmlFile.path, gmlCode, null, gmlFile.kind);
                break;
            case KIND_EVENTS:
                // Post-process
                gmlCode = $hxClasses["file.kind.KGml"].prototype.postproc.call(gmlFile, gmlFile.editor, gmlCode);
                if (gmlCode === null) return;

                // Get Event Data
                var eventData = parsersGmlEvent.parse(gmlCode, $hxClasses["gml.GmlVersion"].v2);
                if (eventData === null) return false;

                var i = 0;
                while(i < eventData.length) {
                    var item = eventData[i];
                    var idat = item.data;
                    var tmp;
                    if (idat.type !== -1) {
                        tmp = true;
                    } else {
                        tmp = (idat.numb !== 1);
                    }

                    if (tmp) {
                        ++i;
                    } else {
                        eventData.splice(i,1);
                    }
                }

                // Build New Syntax Thing-y
                var out = new parsersGmlSeekData;

                var _g = 0;
                while(_g < eventData.length) {
                    var event = eventData[_g++],
                        data = event.data,
                        code = event.code;

                    if (code.length) {
                        var eventName = parsersGmlEvent.toString(data.type, data.numb, data.name);

                        var locals2 = new $hxClasses["gml.GmlLocals"]();
                        out.locals[eventName] = locals2;
                        parsersGmlSeeker.runSyncImpl(gmlFile.path, code[0], null, out, locals2, fileKindYyKYyEvents.inst);
                    }

                }

                // Preserve properties
                out.locals.properties = parsersGmlSeekData.map[gmlFile.path].locals.properties;

                // Merge Syntax Thing-y into existing syntax map
                parsersGmlSeekData.apply(gmlFile.path,parsersGmlSeekData.map[gmlFile.path],out);
                parsersGmlSeekData.map[gmlFile.path] = out;
                out.comps.nameSort();

                break;
            case KIND_ROOMCCS:
                // Post-process
                gmlCode = $hxClasses["file.kind.KGml"].prototype.postproc.call(gmlFile, gmlFile.editor, gmlCode);
                if (gmlCode === null) return;

                // Check Syntax
                parsersGmlSeeker.runSync(gmlFile.path, gmlCode, null, gmlFile.kind);
                break;
            default:
                parsersGmlSeeker.runSync(gmlFile.path, gmlCode, null, gmlFile.kind);
        }

        // Set locals
        var locals = parsersGmlSeekData.map[gmlFile.path].locals;
        if(gmlFile.codeEditor.locals !== locals) {
            gmlFile.codeEditor.locals = locals;
            if($hxClasses["gml.file.GmlFile"].current === gmlFile) {
                gmlFile.codeEditor.session.bgTokenizer.start(0);
            }
        }
    }

    function onchange () {
        if (!aceEditor.session.gmlFile) return;
        if (!aceEditor.session.gmlFile.path) return;

        if (syntaxTimeout) clearTimeout(syntaxTimeout);

        const delay = Math.max(0, parseInt(Orpheus.preferences.liveSyntaxDelay));

        syntaxTimeout = setTimeout(function() {
            // Override alert. Bad bad bad, but I am doing it anyway.
            const oldAlert = window.alert; window.alert = function() {};

            try {
                doSyntaxUpdate(aceEditor.session);
            } finally {
                window.alert = oldAlert;
            }

            syntaxTimeout = null;
        }, delay);
    }

    function init() {
        aceEditor.on("change", onchange);
    }

    function cleanup() {
        aceEditor.off("change", onchange);
    }

    return {
        init: init,
        cleanup: cleanup
    }
})();