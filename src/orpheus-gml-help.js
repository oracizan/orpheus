OrpheusGmlHelp = (function() {
    const FileKind = $gmedit["file.FileKind"];
    const Editor = $gmedit["editors.Editor"];
    const oldProc = $hxClasses["ui.OpenDeclaration"].proc;

    const HelpViewer = function(file1, url) {
        $hxClasses["editors.Editor"].call(this, file1);
        this.url = url;
        this.element = document.createElement("div");
        this.element.style.display = "none";
        Editor.container.appendChild(this.element);

        this.iframe = document.createElement("iframe");
        this.iframe.src = url;
        this.iframe.style.border = "0";
        this.iframe.style.width = "100%";
        this.iframe.style.height = "100%";

        this.element.appendChild(this.iframe);
    };
    HelpViewer.prototype = GMEdit.extend(Editor.prototype, {
        destroy: function() {
            Editor.container.removeChild(this.element);
        },
        focusGain: function(prev) {
            if(prev.element !== this.element) {
                this.element.style.display = "block";
            }
        }
        ,focusLost: function(next) {
            if(next.element !== this.element) {
                this.element.style.display = "none";
            }
        }
    });

    function KHelp() {
        FileKind.call(this);
    }

    KHelp.prototype = GMEdit.extend(FileKind.prototype, {
        init: function(file, data) {
            file.editor = new HelpViewer(file, data);
        }
    });

    KHelp.inst = new KHelp();

    function open(url) {
        var kind = KHelp.inst;
        var _g = 0;
        var _g1 = $hxClasses["ui.ChromeTabs"].element.querySelectorAll(".chrome-tab");
        while(_g < _g1.length) {
            var tab = _g1[_g];
            ++_g;
            if(tab.gmlFile.kind != kind) {
                continue;
            }
            tab.gmlFile.editor.url = url;
            tab.gmlFile.editor.iframe.src = url;
            tab.click();
            return;
        }
        kind.create("GML Help",null,url,null);
    }

    function openDeclarationProc(session, pos, token) {
        var ui_OpenDeclaration = $hxClasses["ui.OpenDeclaration"],
            HxOverrides = $hxClasses["HxOverrides"],
            StringTools = $hxClasses["StringTools"],
            gmx_GmxObject = $hxClasses["gmx.GmxObject"],
            yy__$YyObject_YyObject_$Impl_$ = $hxClasses["yy._YyObject.YyObject_Impl_"],
            gml_file_GmlFile = $hxClasses["gml.file.GmlFile"],
            gml_GmlAPI = $hxClasses["gml.GmlAPI"]

        if (token === null) return false;
        var term = token.value;
        if(token.type.indexOf("importpath") >= 0) {
            if(ui_OpenDeclaration.openImportFile(term.substring(1,term.length - 1))) {
                return true;
            }
        }
        if(HxOverrides.cca(term,0) == 36 || StringTools.startsWith(term,"0x")) {
            ui_ColorPicker.open(term);
            return true;
        }
        if(term.substring(0,2) == "@[") {
            var vals = new RegExp("^@\\[(.*)\\]").exec(term);
            if(vals != null) {
                ui_OpenDeclaration.openLink(vals[1],pos);
            }
            return true;
        }
        if(term == "event_inherited" || term == "action_inherited") {
            var def = session.gmlScopes.get(pos.row);
            if(def == "") {
                return false;
            }
            var file1 = gml_file_GmlFile.current;
            var path = file1.path;
            if(((file1.kind) instanceof file_kind_gmx_KGmxEvents)) {
                return gmx_GmxObject.openEventInherited(path,def) != null;
            } else if(((file1.kind) instanceof file_kind_yy_KYyEvents)) {
                return yy__$YyObject_YyObject_$Impl_$.openEventInherited(path,def) != null;
            } else {
                return false;
            }
        }
        _hx_loop1: while(true) {
            var scope = session.gmlScopes.get(pos.row);
            if(scope == null) {
                break;
            }
            var imp = gml_file_GmlFile.current.codeEditor.imports[scope];
            if(imp == null) {
                break;
            }
            var iter = new AceTokenIterator(session,pos.row,pos.column);
            var tk = iter.stepBackward();
            var next;
            var ns;
            if(tk != null && tk.value == ".") {
                tk = iter.stepBackward();
                if(tk != null) {
                    switch(tk.type) {
                        case "enum":
                            var en = gml_GmlAPI.gmlEnums[tk.value];
                            if(en == null) {
                                break _hx_loop1;
                            }
                            return ui_OpenDeclaration.openLookup(en.fieldLookup[term],{ ctx : term, pos : { column : 0, row : 0}, ctxAfter : true});
                        case "local":
                            var t = imp.localTypes[tk.value];
                            if(t == null) {
                                break _hx_loop1;
                            }
                            ns = imp.namespaces[t];
                            if(ns == null) {
                                break _hx_loop1;
                            }
                            next = ns.longen[term];
                            if(next != null) {
                                term = next;
                            }
                            break _hx_loop1;
                        case "namespace":
                            ns = imp.namespaces[tk.value];
                            if(ns == null) {
                                break _hx_loop1;
                            }
                            next = ns.longen[term];
                            if(next != null) {
                                term = next;
                            }
                            break _hx_loop1;
                    }
                }
            }
            next = imp.longen[term];
            if(next != null) {
                term = next;
            }
            break;
        }
        if(ui_OpenDeclaration.openLocal(term,pos,null)) {
            return true;
        }
        var helpURL = gml_GmlAPI.helpURL;
        if(helpURL != null) {
            var helpLookup = gml_GmlAPI.helpLookup;
            if(helpLookup != null) {
                var helpTerm = helpLookup[term];
                if(helpTerm == null) {
                    helpTerm = helpLookup[StringTools.replace(term,"color","colour")];
                }
                if(helpTerm != null) {
                    open(StringTools.replace(helpURL,"$1",helpTerm));
                    return true;
                }
            } else {
                open(StringTools.replace(helpURL,"$1",term));
                return true;
            }
        }
        open("https://docs2.yoyogames.com/");
        return true;
    }

    function init() {
        $hxClasses["ui.OpenDeclaration"].proc = openDeclarationProc;
    }

    function cleanup() {
        $hxClasses["ui.OpenDeclaration"].proc = oldProc;
    }

    return {
        init: init,
        open: open,
        cleanup: cleanup
    }
})();