OrpheusSearchBox = (function(){
    let searchWrapper = null;

    function init() {
        // Create base element
        searchWrapper = document.createElement("div");
        searchWrapper.id = "orpheus-search-wrapper";

        // Append element to DOM
        const aceEditorWindow = document.querySelector('.bottom.gml');
        aceEditorWindow.parentNode.insertBefore(searchWrapper, aceEditorWindow);

        // Override Ace commands
        aceEditor.commands.addCommand({
            name: "find",
            bindKey: {win: "Ctrl-F", mac: "Command-F"},
            exec: function(editor) {
                ace.define.modules["ext-orpheus-searchbox"].Search(editor);
            }
        });

        aceEditor.commands.addCommand({
            name: "replace",
            bindKey: {win: "Ctrl-H", mac: "Command-H"},
            exec: function(editor) {
                ace.define.modules["ext-orpheus-searchbox"].Search(editor, true);
            }
        });
        aceEditor.commands.addCommand({
            name: "replace",
            bindKey: {win: "Ctrl-R", mac: "Command-R"},
            exec: function(editor) {
                ace.define.modules["ext-orpheus-searchbox"].Search(editor, true);
            }
        });
    }

    function cleanup() {
        if (searchWrapper) searchWrapper.remove();

        // todo: restore ace commands?
    }

    return {
        init: init,
        cleanup: cleanup
    }
})();