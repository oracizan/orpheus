OrpheusTabs = (function(){
    // Create base element
    let pollInterval = null;
    let tabWrapper = null;

    function onAllTabsEvent(e) {
        rebuildTabs(e.target.tabEls);
    }

    function tabElement(chromeTab) {
        const tab = document.createElement("div");
        tab.className = "orpheus-tab";
        if (chromeTab.classList.contains('chrome-tab-current')) tab.classList.add("orpheus-tab-current");
        if (chromeTab.classList.contains('chrome-tab-changed')) tab.classList.add("orpheus-tab-changed");
        if (chromeTab.classList.contains('chrome-tab-pinned')) tab.classList.add("orpheus-tab-pinned");
        if (chromeTab.classList.contains('chrome-tab-idle')) tab.classList.add("orpheus-tab-idle");

        tab.innerHTML = chromeTab.innerText;
        tab.chromeTab = chromeTab;

        if (chromeTab.gmlFile) {
            if (chromeTab.gmlFile.kind instanceof $hxClasses["file.kind.gml.KGmlMultifile"]) {
                tab.innerHTML = '<i class="fas fa-folder"></i>&nbsp;' + chromeTab.innerText;
            }
        }

        const tabClose = document.createElement("div");
        tabClose.className = 'orpheus-tab-close';
        tab.appendChild(tabClose);
        return tab;
    }

    function pollTabs() {
        // Skip if tabs are being interacted with
        if (document.querySelector('.orpheus-tab-chosen')) return;

        const tabs = tabWrapper.children;
        let lastPinnedIndex = -1;
        let tabsShuffled = false;

        for (let i = 0; i < tabs.length; i++) {
            const tab = tabs[i];

            if (tabs[i].chromeTab) { //chromeTab is undefined sometimes. I'm not sure why.
                // Changed
                const tabChanged = tab.classList.contains("orpheus-tab-changed");
                const chromeTabChanged = tab.chromeTab.classList.contains('chrome-tab-changed');
                if (tabChanged !== chromeTabChanged) tab.classList.toggle("orpheus-tab-changed", chromeTabChanged);

                // Idle
                const tabIdle = tab.classList.contains("orpheus-tab-idle");
                const chromeTabIdle = tab.chromeTab.classList.contains('chrome-tab-idle');
                if (tabIdle !== chromeTabIdle) tab.classList.toggle("orpheus-tab-idle", chromeTabIdle);

                // Pinned
                const tabPinned = tab.classList.contains("orpheus-tab-pinned");
                const chromeTabPinned = tab.chromeTab.classList.contains('chrome-tab-pinned');
                if (tabPinned !== chromeTabPinned) {
                    tab.classList.toggle("orpheus-tab-pinned", chromeTabPinned);
                }
            }

            // Make sure all pinned tabs are first
            if (tab.classList.contains("orpheus-tab-pinned")) {
                if (i - lastPinnedIndex > 1) {
                    $gmedit["ui.ChromeTabs"].impl.animateTabMove(tab.chromeTab, i, lastPinnedIndex + 1);
                    lastPinnedIndex = lastPinnedIndex + 1;
                    tabsShuffled = true;
                } else {
                    lastPinnedIndex = i;
                }
            }
        }
        if (tabsShuffled) {
            GMEdit._emit("tabsReorder", {target: $gmedit["ui.ChromeTabs"].impl});
        }
    }

    function rebuildTabs(tabEls) {
        if (!tabEls) tabEls = $gmedit["ui.ChromeTabs"].impl.tabEls;
        tabWrapper.innerHTML = "";
        for (let i = 0; i < tabEls.length; i++) {
            let tab = tabElement(tabEls[i]);
            tabWrapper.appendChild(tab);
        }
    }

    function init() {
        // Add class to body
        document.body.classList.add("orpheus-tabs");

        // Add wrapper to DOM
        tabWrapper = document.createElement("div");
        tabWrapper.id = "orpheus-tab-wrapper";
        const chromeTabs = document.querySelector('#tabs');
        chromeTabs.parentNode.insertBefore(tabWrapper, chromeTabs.nextSibling);

        // Event Listeners
        $gmedit["ui.ChromeTabs"].impl.el.addEventListener('tabAdd', onAllTabsEvent);
        $gmedit["ui.ChromeTabs"].impl.el.addEventListener('tabRemove', onAllTabsEvent);
        $gmedit["ui.ChromeTabs"].impl.el.addEventListener('activeTabChange', onAllTabsEvent);
        GMEdit.on("tabsReorder", e => {
            if (!e.orpheusTabsIgnore) onAllTabsEvent(e)
        });

        // Poll (no event fired when tab is pinned / file has changed?)
        pollInterval = setInterval(pollTabs, 100);

        // Tab Event listeners
        tabWrapper.addEventListener('click', function (event) {
            if (event.target.classList.contains('orpheus-tab')) {
                const newEvent = new MouseEvent('click', {
                    ctrlKey: event.ctrlKey,
                    bubbles: true,
                    cancelable: true,
                    view: window
                });
                event.target.chromeTab.dispatchEvent(newEvent);
            }

            if (event.target.classList.contains('orpheus-tab-close')) {
                event.target.parentNode.chromeTab.querySelector('.chrome-tab-close').click();
            }
        });

        tabWrapper.addEventListener('contextmenu', function (event) {
            if (event.target.classList.contains('orpheus-tab')) {
                event.target.chromeTab.dispatchEvent(new CustomEvent('contextmenu'));
            }
        });

        // Sortable
        Sortable.create(tabWrapper, {
            ghostClass: 'orpheus-tab-ghost',
            chosenClass: 'orpheus-tab-chosen',
            dragClass: 'orpheus-tab-drag',
            forceFallback: true,
            fallbackTolerance: 3,
            onEnd: function(e) {
                if (e.oldIndex === e.newIndex) return;
                $gmedit["ui.ChromeTabs"].impl.animateTabMove(e.item.chromeTab, e.oldIndex, e.newIndex);
                e.item.chromeTab.click();
                GMEdit._emit("tabsReorder", {target: $gmedit["ui.ChromeTabs"].impl, orpheusTabsIgnore: true});
                //pollTabs();
            }
        });

        // First build
        rebuildTabs();
    }

    function cleanup () {
        if (tabWrapper) tabWrapper.remove();
        if (pollInterval) clearInterval(pollInterval);
        document.body.classList.remove("orpheus-tabs");
    }

    return {
        init: init,
        cleanup: cleanup
    }

})();