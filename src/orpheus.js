Orpheus = (function() {

    const version = '1.0.12';
    const preferencesPath = Electron_App.getPath("userData") + "/GMEdit/config/Orpheus-preferences.json";
    let preferences = {
        useGmlHelp: true,
        useOutlineView: true,
        useTabs: true,
        useLiveSyntax: true,
        useSearchBox: true,
        liveSyntaxDelay: 100
    };

    function savePreferences() {
        Electron_FS.writeFileSync(preferencesPath, JSON.stringify(preferences));
    }

    function loadPreferences() {
        preferences = Object.assign(preferences, JSON.parse(Electron_FS.readFileSync(preferencesPath)));
    }

    function buildPreferencesUi(preferencesElement) {
        const uiPreferences = $gmedit["ui.Preferences"];

        uiPreferences.addText(preferencesElement, "").innerHTML = "<b>Orpheus v" + version + " by Oracizan</b>";
        uiPreferences.addText(preferencesElement, "").innerHTML = "<i>GMEdit will need to be restarted for changes to apply.</i>";
        uiPreferences.addText(preferencesElement, " ");
        uiPreferences.addCheckbox(preferencesElement, "Open GMS Manual in GMEdit", preferences.useGmlHelp, (value) => { preferences.useGmlHelp = value; savePreferences(); });
        uiPreferences.addCheckbox(preferencesElement, "Override Outline View", preferences.useOutlineView, (value) => { preferences.useOutlineView = value; savePreferences(); });
        uiPreferences.addCheckbox(preferencesElement, "Override Tabs", preferences.useTabs, (value) => { preferences.useTabs = value; savePreferences(); });
        uiPreferences.addCheckbox(preferencesElement, "Override Find & Replace", preferences.useSearchBox, (value) => { preferences.useSearchBox = value; savePreferences(); });
        uiPreferences.addCheckbox(preferencesElement, "Use Live Syntax", preferences.useLiveSyntax, (value) => { preferences.useLiveSyntax = value; savePreferences(); });
        uiPreferences.addInput(preferencesElement, "Live Syntax Delay (ms)", preferences.liveSyntaxDelay, (value) => { preferences.liveSyntaxDelay = value; savePreferences(); });
    }

    function init() {
        // Load preferences file
        if (Electron_FS.existsSync(preferencesPath)) {
            try {
                loadPreferences();
            } catch(e) {
                console.error("Orpheus - Failed to read or parse preferences file.");
            }
        } else {
            savePreferences();
        }

        // Initialize components
        if (preferences.useGmlHelp) OrpheusGmlHelp.init();
        if (preferences.useTabs) OrpheusTabs.init();
        if (preferences.useOutlineView) OrpheusOutlineView.init();
        if (preferences.useLiveSyntax) OrpheusLiveSyntax.init();
        if (preferences.useSearchBox) OrpheusSearchBox.init();

        // Build Preferences UI
        const el = document.querySelector('.plugin-settings[for="orpheus"]');

        if (el) {
            buildPreferencesUi(el);
        } else {
            GMEdit.on("preferencesBuilt", (e) => {
                buildPreferencesUi(e.target.querySelector('.plugin-settings[for="orpheus"]'));
            });
        }
    }

    function cleanup () {
        OrpheusGmlHelp.cleanup();
        OrpheusTabs.cleanup();
        OrpheusOutlineView.cleanup();
        OrpheusLiveSyntax.cleanup();
        OrpheusSearchBox.cleanup();
    }

    GMEdit.register("orpheus", {
        init: init,
        cleanup: cleanup
    });

    return {
        preferences: preferences
    }
})();

