# Orpheus

Orpheus is a plugin + theme for [GMEdit](https://yellowafterlife.itch.io/gmedit). It does not aim to serve a single well-defined purpose; rather, it offers a variety of quality of life tweaks which have been lumped together in one place for my own convenience. These tweaks include the following:

<dl>
  <dd><b>GMS2 Manual</b><br />Allow [the GMS2 Manual](https://docs2.yoyogames.com/) to be opened inside a GMEdit tab.</dd>
  <dd><b>Resource Tree</b><br />Increase the font size and add custom FontAwesome resource icons.</dd>
  <dd><b>Tabs</b><br />Override the default Chrome styled tabs with PhpStorm styled multi-row tabs.</dd>
  <dd><b>Find & Replace</b><br />Override the default find and replace bar with one that sits above (and pushes down) the editor.</dd>
  <dd><b>Outline View</b><br />Activate the 'current file only' outline mode, and only show the outline panel if the currently edited file has sub-items (e.g. #event or #region tags).</dd>
  <dd><b>Live Syntax Highlighting</b><br />Highlight GML syntax as it is typed, instead of just upon save. <i>(Experimental)</i></dd>
</dl>

Any features that are not to your taste can be toggled off in GMEdit's Preferences menu. The Resource Tree changes can be toggled off by using a non-Orpheus theme.

## Setup

1. Download and extract this repository.
2. Create a directory named **orpheus** inside of your GMEdit plugins directory. Copy all of the files from the **src** directory to this newly created directory (do not copy the **src** directory itself).
3. Create a directory named **orpheus** inside of your GMEdit theme directory. Copy all of the files from the **theme** directory to this newly created directory (do not copy the **theme** directory itself).
4. Restart GMEdit.

For more help, please see the GMEdit wiki entries for [using plugins](https://github.com/GameMakerDiscord/GMEdit/wiki/Using-plugins) and [using themes](https://github.com/GameMakerDiscord/GMEdit/wiki/Using-themes).

## Screenshot

![screenshot](https://i.imgur.com/TrYAPAx.png)

## Credits

+ [Sortable](https://github.com/SortableJS/Sortable) for tab dragging functionality.
+ [FontAwesome](https://fontawesome.com/) for resource tree icons.

## Thanks

+ [YellowAfterLife](https://twitter.com/YellowAfterlife) for making GMEdit in the first place.
+ [Nommiin](https://twitter.com/nommiin) for making the [builder plugin](https://github.com/nommiin/builder), which I referenced while building this plugin.